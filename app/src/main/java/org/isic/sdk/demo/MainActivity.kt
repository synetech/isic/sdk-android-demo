package org.isic.sdk.demo

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.runBlocking
import org.isic.sdk.ISIC
import org.isic.sdk.domain.model.DiscountsWidgetEndpoint
import org.isic.sdk.domain.model.DiscountsWidgetTheme
import org.isic.sdk.domain.model.IsicEndpoint
import org.isic.sdk.presentation.ui.activity.DiscountsWidgetActivity
import org.isic.sdk.presentation.ui.activity.HostActivity
import java.io.ByteArrayOutputStream

class MainActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initIsicSdk()
        initIsicDiscountsWidget()
        setButtonListeners()
    }

    override fun onDestroy() {
        ISIC.destroy()
        super.onDestroy()
    }

    private fun setButtonListeners() {
        findViewById<Button>(R.id.btn_verify_card)?.setOnClickListener {
            verifyCard()
        }
        findViewById<Button>(R.id.btn_upload_photo)?.setOnClickListener {
            uploadPhoto()
        }
        findViewById<Button>(R.id.btn_live)?.setOnClickListener {
            startActivity(Intent(this, HostActivity::class.java))
        }
        findViewById<Button>(R.id.btn_clear)?.setOnClickListener {
            clear()
        }
        findViewById<Button>(R.id.btn_simulated)?.setOnClickListener {
            ISIC.initializeMockedSdk(this)
            startActivity(Intent(this, HostActivity::class.java))
        }
        findViewById<Button>(R.id.btn_discounts_widget)?.setOnClickListener {
            startActivity(Intent(this, DiscountsWidgetActivity::class.java))
        }
    }

    private fun initIsicSdk() {
        ISIC.initialize(
            context = this,
            clientId = "isic-sdk-demo-app",
            clientSecret = "1809b8e0-5f1e-4139-863f-e9c58c7c8ef0",
            redirectUri = "isic-sdk-demo-app.isic.org",
            endpoint = IsicEndpoint.STAGING,
            showLogs = BuildConfig.DEBUG,
        )
    }

    private fun initIsicDiscountsWidget() {
        ISIC.initializeDiscountsWidget(
            accessToken = "130e9596-24a0-4fda-a1ee-829a43102efe",
            applicationName = "staging-isic-benefit-search",
            endpoint = DiscountsWidgetEndpoint.STAGING,
            theme = DiscountsWidgetTheme(primaryColor = "#40b8b8", secondaryColor = "#346562")

        )
    }

    private fun verifyCard() {
        // handle disposable
        ISIC.verifyCard("S420900017560X", "Fish Breath Ted")
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe({
                toast("Card verified")
            }, {
                toast("Card verification failed")
                Log.e("MainActivity", "Card verification failed", it)
            })
    }

    private fun uploadPhoto() {
        val drawable = ResourcesCompat.getDrawable(resources, org.isic.feature.sdk.R.drawable.img_error_unknown, null)!!

        val bitmap: Bitmap = (drawable as BitmapDrawable).bitmap
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream)
        val bytes: ByteArray = stream.toByteArray()

        // handle disposable
        ISIC.verifyCard("S420900017560X", "Fish Breath Ted")
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .flatMapCompletable { virtualIdInfo ->
                if (virtualIdInfo.hasPhoto) {
                    Completable.complete()
                } else {
                    ISIC.uploadPhoto(bytes)
                }
            }
            .subscribe({
                toast("Photo uploaded")
            }, {
                toast("Photo upload failed")
                Log.e("MainActivity", "Photo upload failed", it)
            })
    }

    private fun clear() {
        runBlocking {
            ISIC.clear({
                toast("Cleared successfully")
            }, {
                toast("Clear failed")
                Log.e("MainActivity", "Clear failed", it)
            }, this)
        }
    }

    private fun toast(message: String) {
        runOnUiThread {
            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
        }
    }
}