package org.isic.sdk.demo

import android.app.Activity
import android.content.Intent
import android.os.Bundle

class SplashActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        onSplashProceed()
    }

    private fun onSplashProceed() {
        startActivity(Intent(this, MainActivity::class.java))
    }
}