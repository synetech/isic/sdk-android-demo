ISIC 3rd part SDK demo app
=================

This project serves two purposes:
- demo of the ISIC Android SDK
- example of implementing the SDK

### Client auth
In order to use the SDK, client ID and client secret have to be obtained from ISIC. These can be supplied by ISIC ServiceOffice, please contact mobile@isic.org to request one.

### Overview
The SDK consists of 3 main parts: card verification, profile photo upload and the ISIC card. First two parts can be replaced with your own implementation.

## Card verification
A sort of "login" screen. User provides name and ISIC card number and is verified by the backend. Successful request returns `VirtualIdInfo` that contains all important information about the user.

## Profile photo upload
At first start, the user probably will not have a profile photo ready. The user can choose a photo from gallery or take a new photo, which is then cropped and compressed before uploading to server.

## ISIC card
The main screen of the SDK that shows the ISIC card(s). For security reasons, this screen cannot be replaced.

### How to implement
1. Add our GitLab repository to your subproject's repositories
```groovy
maven {
    name "Synetech GitLab"
    url "https://gitlab.com/api/v4/projects/26360154/packages/maven"
}
```
2. Add the Android SDK as dependency
```groovy
implementation("org.isic.sdk:sdk-android:1.3.0.0")
```
3. Declare the SDK's host activity in `AndroidManifest.xml`
```xml
<activity
    android:name="org.isic.sdk.presentation.ui.activity.HostActivity"
    android:label="@string/app_name"
    android:screenOrientation="portrait"
    android:theme="@style/AppIsicTheme"
    tools:replace="android:theme" />
```
4. Initialize the SDK
```kotlin
ISIC.initialize(
    context = this,
    clientId = "<clientId>",
    clientSecret = "<clientSecret>",
    redirectUri = "<redirectUri>",
    endpoint = IsicEndpoint.STAGING,
    showLogs = BuildConfig.DEBUG,
)
```
5. Start the `HostActivity`.
```kotlin
startActivity(Intent(this, HostActivity::class.java))
```
6. Destroy the SDK with your app.
```kotlin
override fun onDestroy() {
    ISIC.destroy()
    super.onDestroy()
}
```

### Background requests
If you do not wish to show/use the SDK's card validation and profile photo upload screens, you can implement your own if you so desire. Before starting the `HostActivity`, call the requests `verifyCard` and `uploadPhoto` (if needed), respectively. Both coroutines and rxjava2 functions are implemented. To see an example (without handling disposables), find `MainActivity` in this project. `verifyCard` request returns `VirtualIdInfo`, which contains `hasPhoto` property, that tells you whether the user has a valid profile photo.

### Expired card actions

## Discounts widget
The second screen of the SDK that shows the ISIC discounts.

### How to implement
1. Add our GitLab repository to your subproject's repositories
```groovy
maven {
    name "Synetech GitLab"
    url "https://gitlab.com/api/v4/projects/26360154/packages/maven"
}
```
2. Add the Android SDK as dependency
```groovy
implementation("org.isic.sdk:sdk-android:1.3.0.0")
```
3. Declare the SDK's discounts widget activity in `AndroidManifest.xml`
```xml
<activity
    android:name="org.isic.sdk.presentation.ui.activity.DiscountsWidgetActivity"
    android:label="@string/app_name"
    android:screenOrientation="portrait"
    android:theme="@style/DiscountsWidgetTheme"
    tools:replace="android:theme" />
```
4. Initialize the discounts widget
```kotlin
ISIC.initializeDiscountsWidget(
    accessToken = "<accessToken>",
    applicationName = "<applicationName>",
    endpoint = DiscountsWidgetEndpoint.STAGING,
    locale = "<language code, default 'en'>",
    defaultLocation = "<City or country>",
    theme = DiscountsWidgetTheme(primaryColor = "#40b8b8", secondaryColor = "#346562"),
    translations = "<string containing translation json>"
)
```
5. Start the `DiscountsWidgetActivity`.
```kotlin
startActivity(Intent(this, DiscountsWidgetActivity::class.java))
```
6. Destroy the SDK with your app.
```kotlin
override fun onDestroy() {
    ISIC.destroy()
    super.onDestroy()
}
```

# Customization

## Strings

## Colors

## Fonts
