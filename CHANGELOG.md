# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

*Guiding Principles:*
* Changelogs are for humans, not machines.
* There should be an entry for every single version.
* The same types of changes should be grouped.
* Versions and sections should be linkable.
* The latest version comes first.
* The release date of each version is displayed.

*Types of changes:*
* **Added** for new features.
* **Changed** for changes in existing functionality.
* **Deprecated** for soon-to-be removed features.
* **Removed** for now removed features.
* **Fixed** for any bug fixes.
* **Security** in case of vulnerabilities.

## Unreleased

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

## 1.3.0 - 19.8.2022

### Changed
- Update SDK to v1.3.0

## 1.1.0 - 25.1.2022

### Added
- Added Discounts widget

### Changed
- updated sdk to 1.1.0.0
- updated README

## 1.0.0 - 16.9.2021

### Added
- Firebase app distribution
- Firebase crashlytics
- Buttons for extra background actions
- README
- redirectUri parameter

### Changed
- splash
- app icon
- updated sdk to 0.1.0.7
- package name to org.isic

### Removed
- unused feature modules
- base submodule from demo app
